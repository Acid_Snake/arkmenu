#ifndef CSO_H
#define CSO_H

#include <zlib.h>
#include "iso.h"

#define CSO_MAGIC 0x4F534943

typedef struct
{
	unsigned magic;
	unsigned header_size;
	unsigned long long file_size;
	unsigned block_size;
	unsigned char version;
	unsigned char align;
	char reserved[2];
} __attribute__ ((__packed__)) cso_header;

class Cso : public Iso
{
	public:

		Cso(string path);
		~Cso();

		void loadIcon();
		void getTempData1();
		void getTempData2();

		int open(const char * path);
		
		int readFile(const char* file, char** destination);
		
		static bool isPatched(string path);
		static bool isCSO(const char* filepath);

	private:

		void clear();
		int decompress_sector(unsigned sector);
		unsigned get_block_offset(unsigned sector);
	
		//overrides some iso methods
		int read(void * destination, unsigned size, unsigned count, void * p);
		int seek(void * p, unsigned bytes, int flag);

		unsigned current_sector;
		unsigned char * read_buffer;
		unsigned char * buffer;
		unsigned char * buffer_pointer;
		cso_header info;
		z_stream dec;
};

#endif
