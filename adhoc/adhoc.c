#include <pspsdk.h>
#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <pspnet_adhocmatching.h>
#include <psputility.h>
#include <pspnet.h>
#include <pspkernel.h>
#include <pspdisplay.h>
#include <string.h>
#include <math.h>
#include <pspgu.h>
#include <pspgum.h>
#include <pspnet_adhoc.h>
#include <pspnet_adhocctl.h>
#include <psputility_gamesharing.h>

#include "adhoc.h"

#define _NET_ADHOCCTL_EVENT_ERROR		0
#define _NET_ADHOCCTL_EVENT_CONNECT		1
#define _NET_ADHOCCTL_EVENT_DISCONNECT	2
#define _NET_ADHOCCTL_EVENT_SCAN			3
#define _NET_ADHOCCTL_EVENT_GAMEMODE		4

static int AdhocErrorCode = 0;
static int AdhocEventFlag = 0;

static int AdhocMatchingId = 0;
static int AdhocPtpHostId = 0;
static int AdhocPtpClientId = 0;

static char AdhocctlGroupName[8];

static AdhocPeerEvent AdhocPeerEvents[15];

static void AdhocctlHandler(int event, int error, void *arg)
{
	(void)arg;
	
	if(event == _NET_ADHOCCTL_EVENT_ERROR)
	{
		AdhocErrorCode = error;
		AdhocEventFlag |= _ADHOC_EVENT_ERROR;
	}	
	else if(event == _NET_ADHOCCTL_EVENT_CONNECT)
	{
		AdhocErrorCode = 0;
		AdhocEventFlag |= _ADHOC_EVENT_CONNECT;
	}
	else if(event == _NET_ADHOCCTL_EVENT_DISCONNECT)
	{
		AdhocErrorCode = 0;
		AdhocEventFlag |= _ADHOC_EVENT_DISCONNECT;
	}
	else if(event == _NET_ADHOCCTL_EVENT_SCAN)
	{
		AdhocErrorCode = 0;
		AdhocEventFlag |= _ADHOC_EVENT_SCAN;
	}	
	else if(event == _NET_ADHOCCTL_EVENT_GAMEMODE)
	{
		AdhocErrorCode = 0;
		AdhocEventFlag |= _ADHOC_EVENT_GAMEMODE;
	}
	
	return;
}

int AdhocPtpInit(int type)
{		
	int result;
	
	result = sceUtilityLoadNetModule(PSP_NET_MODULE_COMMON);
	
	if(result < 0)
	{
		printf("Adhoc error: sceUtilityLoadNetModule(PSP_NET_MODULE_COMMON): 0x%08X", result);
		return result;
	}
	
	result = sceUtilityLoadNetModule(PSP_NET_MODULE_ADHOC);
	
	if(result < 0)
	{
		printf("Adhoc error: sceUtilityLoadNetModule(PSP_NET_MODULE_ADHOC); 0x%08X", result);
		return result;
	}
	
	result = sceNetInit(128*1024, 42, 4*1024, 42, 4*1024);
	
	if(result < 0)
	{
		printf("Adhoc error: sceNetInit(); 0x%08X", result);
		return result;
	}
	
	result = sceNetAdhocInit();
	
	if(result < 0)
		printf("Adhoc error: sceNetAdhocInit(): 0x%08X", result);
	
	struct productStruct gameProduct;
	
	gameProduct.unknown = type;
	
	if(type == _ADHOC_TYPE_GAMESHARING)
		memcpy(gameProduct.product, "000000001", 9);
	else
		memcpy(gameProduct.product, "ULUS99999", 9);
	
	result = sceNetAdhocctlInit(32*1024, 0x20, &gameProduct);
	
	if(result < 0)
		printf("Adhoc error: sceNetAdhocctlInit(): 0x%08X", result);
	
	// Register pspnet_adhocctl event handler
	result = sceNetAdhocctlAddHandler(AdhocctlHandler, NULL);
	
	if(result < 0)
	{
		printf("Adhoc error: sceNetAdhocctlAddHandler\n");
		return 0;
	}
	
	return 1;
}

int AdhocPtpShutdown(void)
{	
	sceNetAdhocctlDisconnect();
	
	sceNetAdhocctlDelHandler(0);
	
	sceNetAdhocctlTerm(); 
	
	sceNetAdhocTerm();
	
	sceNetTerm();
	
	sceUtilityUnloadNetModule(PSP_NET_MODULE_ADHOC);
	
	sceUtilityUnloadNetModule(PSP_NET_MODULE_COMMON);
	
	return 1;
}

int AdhocPtpGetState(void)
{
	if(AdhocEventFlag & _ADHOC_EVENT_ERROR)
	{
		AdhocEventFlag &= ~_ADHOC_EVENT_ERROR;
		return(_ADHOC_EVENT_ERROR);
	}
	else if(AdhocEventFlag & _ADHOC_EVENT_CONNECT)
	{
		AdhocEventFlag &= ~_ADHOC_EVENT_CONNECT;
		return(_ADHOC_EVENT_CONNECT);
	}
	else if(AdhocEventFlag & _ADHOC_EVENT_DISCONNECT)
	{
		AdhocEventFlag &= ~_ADHOC_EVENT_DISCONNECT;
		return(_ADHOC_EVENT_DISCONNECT);
	}
	else if(AdhocEventFlag & _ADHOC_EVENT_SCAN)
	{
		AdhocEventFlag &= ~_ADHOC_EVENT_SCAN;
		return(_ADHOC_EVENT_SCAN);
	}
	else if(AdhocEventFlag & _ADHOC_EVENT_GAMEMODE)
	{
		AdhocEventFlag &= ~_ADHOC_EVENT_GAMEMODE;
		return(_ADHOC_EVENT_GAMEMODE);
	}
	else if(AdhocEventFlag & _ADHOC_EVENT_CANCEL)
	{
		AdhocEventFlag &= ~_ADHOC_EVENT_CANCEL;
		return(_ADHOC_EVENT_CANCEL);
	}
	
	return 0;
}

int AdhocPtpHostStart(void)
{
	unsigned char mac[6];
	unsigned char clientmac[6];
	unsigned short clientport;
	
	AdhocPtpHostId = -1;
	AdhocPtpClientId = -1;
	
	int ret = sceNetGetLocalEtherAddr(mac);
		
	if(ret < 0)
	{
		printf("Adhoc error: sceNetGetLocalEtherAddr\n");
		return 0;
	}
	
	ret = sceNetAdhocPtpListen(mac, 1, 8192, 200*1000, 300, 1, 0);
	
	if(ret < 0)
	{
		printf("Adhoc error: sceNetAdhocPtpListen 0x%08X\n", ret);
		
		if(AdhocPtpHostId > 0)
			sceNetAdhocPtpClose(AdhocPtpHostId, 0);
			
		if(AdhocPtpClientId > 0)
			sceNetAdhocPtpClose(AdhocPtpClientId, 0);
		
		return ret;
	}
	
	AdhocPtpHostId = ret;
		
	ret = sceNetAdhocPtpAccept(AdhocPtpHostId, &clientmac[0], &clientport, 0, 0);
	
	if(ret < 0)
	{
		printf("Adhoc error: sceNetAdhocPtpAccept\n");
		
		if(AdhocPtpHostId > 0)
			sceNetAdhocPtpClose(AdhocPtpHostId, 0);
			
		if(AdhocPtpClientId > 0)
			sceNetAdhocPtpClose(AdhocPtpClientId, 0);
		
		return ret;
	}
	
	AdhocPtpClientId = ret;
	
	return ret;
}

int AdhocPtpClientStart(const char *servermac)
{
	unsigned char mac[6];
	
	sceNetEtherStrton(servermac, mac);
	
	unsigned char clientmac[6];
	
	int ret = sceNetGetLocalEtherAddr(clientmac);
	
	if(ret < 0)
	{
		printf("Adhoc error: sceNetGetLocalEtherAddr\n");
		return ret;
	}
	
	ret = sceNetAdhocPtpOpen(clientmac, 0, mac, 1, 8192, 200*1000, 300, 0);
	
	if(ret < 0)
	{
		printf("Adhoc error: sceNetAdhocPtpOpen\n");
		
		if(AdhocPtpClientId > 0)
			sceNetAdhocPtpClose(AdhocPtpClientId, 0);
		
		return ret;
	}
	
	AdhocPtpClientId = ret;
	
	while(1)
	{	
		ret = sceNetAdhocPtpConnect(AdhocPtpClientId, 0, 1);
			
		if(ret < 0 && ret != (int)0x80410709)
		{
			printf("Adhoc error: sceNetAdhocPtpConnect 0x%08X\n", ret);
		
			if(AdhocPtpClientId > 0)
				sceNetAdhocPtpClose(AdhocPtpClientId, 0);
		
			return ret;
		}
		
		if(ret == 0)
			break;
		
		sceKernelDelayThread(200*1000);
	}
	
	return ret;
}

int AdhocPtpReceive(void *data, int *length)
{	
	int ret = sceNetAdhocPtpRecv(AdhocPtpClientId, data, length, 1000*1000, 0);
	
	if(ret < 0 && ret != (int)0x80410709)
	{
		printf("Adhoc error: sceNetAdhocPtpRecv\n");
		
		if(AdhocPtpClientId > 0)
			sceNetAdhocPtpClose(AdhocPtpClientId, 0);
		
		return 0;
	}
	
	return 1;
}

int AdhocPtpSend(void *data, int *length)
{
	int ret = sceNetAdhocPtpSend(AdhocPtpClientId, data, length, 0, 0);
	
	if(ret < 0)
	{
		printf("Adhoc error: sceNetAdhocPtpSend\n");
		
		if(AdhocPtpHostId > 0)
			sceNetAdhocPtpClose(AdhocPtpHostId, 0);
			
		if(AdhocPtpClientId > 0)
			sceNetAdhocPtpClose(AdhocPtpClientId, 0);
		
		return(0);
	}
		
	return 1;
}

int AdhocPtpCheckForData(void)
{
	//int torecv = 0;
	
	int ret, buflen;
	
	//ptpStatStruct *buf, *ptr;
	
	ret = sceNetAdhocGetPtpStat(&buflen, NULL);
	
	if(ret < 0)
		return 0;
	else if (buflen == 0)
		return 0;
		
/*buf = malloc(buflen);
if (buf == NULL)
{
	// Could not allocate memory
	return;
}

ret = sceNetAdhocGetPtpStat(&buflen, buf);
if (ret < 0)
{
	// Error handling
}
else if (buflen == 0)
{
	// No data
}        
else
{
	for (ptr = buf; ptr != NULL; ptr = ptr->next)
	{
		// Process acquired control block
		...
	}
}*/
	
	return 1;
}

int AdhocPtpFlush(void)
{
	int ret = sceNetAdhocPtpFlush(AdhocPtpClientId, 0, 0);
	
	if(ret < 0)
	{
		printf("Adhoc error: sceNetAdhocPtpFlush\n");
		
		if(AdhocPtpHostId > 0)
			sceNetAdhocPtpClose(AdhocPtpHostId, 0);
			
		if(AdhocPtpClientId > 0)
			sceNetAdhocPtpClose(AdhocPtpClientId, 0);
		
		return 0;
	}
	
	return 1;
}

int AdhocPtpHostShutdown(void)
{
	if(AdhocPtpHostId > 0)
		sceNetAdhocPtpClose(AdhocPtpHostId, 0);
			
	if(AdhocPtpClientId > 0)
		sceNetAdhocPtpClose(AdhocPtpClientId, 0);
	
	return 1;
}

int AdhocPtpClientShutdown(void)
{
	if(AdhocPtpClientId > 0)
		sceNetAdhocPtpClose(AdhocPtpClientId, 0);
	
	return 1;
}

int AdhocGetError(void)
{
	return(AdhocErrorCode);
}
