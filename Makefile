TARGET = ARKMENU
CFLAGS = -O2 -G0 -Wall -Wno-unused -Wno-write-strings -Wno-sign-compare
CXXFLAGS = $(CFLAGS) -fno-exceptions -fno-rtti
ASFLAGS = $(CFLAGS)
INCDIR = include include/anim pmf ftp graphics graphics/ya2d graphics/intraFont graphics/jpeg
LIBDIR = libs libs/png
LIBS = -lunziprar -lstdc++ -lya2d -ljpeg -lintraFont -lpng -lpspvfpu -lpspmpeg -lpspaudio -lpspatrac3 -lpspwlan -lpsppower -lpspgum -lpspgu -lz -lm -lpspvram -lpspmath -lpspumd -lpspinit -lpspmp3 -lpsploadexec_kernel -lpspsystemctrl_user
LDFLAGS =
EXTRA_TARGETS = EBOOT.PBP
PSP_EBOOT_TITLE = arkMenu
PSP_EBOOT_ICON = ICON0.PNG
#PSP_EBOOT_ICON1 = ICON1.PMF
#PSP_EBOOT_SND0 = SND0.AT3
PSP_FW_VERSION=371

OBJS = \
		main.o \
		pmf/pmf.o \
		pmf/pmf_video.o \
		pmf/pmf_decoder.o \
		pmf/pmf_audio.o \
		pmf/pmf_reader.o \
		pmf/at3.o \
		ftp/psp_main.o \
		ftp/util.o \
		ftp/loadutil.o \
		ftp/ftpd.o \
		ftp/ftp.o \
		ftp/sutils.o \
		ftp/psp_init.o \
		ftp/psp_cfg.o \
		src/debug.o \
		src/common.o \
		src/gamemgr.o \
		src/entry.o \
		src/iso.o \
		src/cso.o \
		src/eboot.o \
		src/menu.o \
		src/controller.o \
		src/ya2d++.o \
		src/mp3.o \
		src/zip.o \
		src/browser.o \
		src/osk.o \
		src/plugin.o \
		src/pluginmgr.o \
		src/vshmenu.o \
		src/optionsMenu.o \
		src/anim/anim.o \
		src/anim/pixel.o \
		src/anim/wave.o \
		src/anim/sprites.o \
		src/anim/fire.o \
		src/anim/tetris.o \
		src/anim/matrix.o


PSPSDK=$(shell psp-config --pspsdk-path)
include $(PSPSDK)/lib/build.mak

copy: EBOOT.PBP
	#mkdir -p /media/$(USER)/disk/PSP/GAME/$(notdir $(CURDIR))
	$(Q)cp EBOOT.PBP /home/$(USER)/PS Vita/PSAVEDATA/63abc9e624956660/ULES01515TRIAL00/VBOOT.PBP
	sync
distclean:
